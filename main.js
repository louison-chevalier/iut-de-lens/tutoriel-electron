const { app, BrowserWindow } = require('electron')

function createWindow () {
    // Cree la fenetre du navigateur.
    let win = new BrowserWindow({ width: 1000, height: 600 })

    // and load the index.html of the app.
    //win.loadFile('index.html')
    win.loadURL('https://etd-solutions.com')
}

app.on('ready', createWindow)