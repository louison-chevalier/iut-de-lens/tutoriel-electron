# Electron


Electron

Make your first electron app

‌

Init


========

‌

*   Create your project directory in your code editor.

*   In your terminal.

    your-project-directory: npm init -y your-project-directory: npm install electron && npm install electron -g

*   Add in the directory a new file `electron.js`.

*   Open the `package.json` file and replace `index.js` to `electron.js` at the main property.

*   The project directory structure :

    your-project-directory |-- electron.js |-- package-lock.json |-- package.json


‌

Launch a website


====================

‌

*   Open the `electron.js` file

*   First of all, we have to import the Electron modules :

    const { app, BrowserWindow, ipcMain } = require('electron')


> The goal of this sheet is to create a window in which there is a webpage.

let window

const createWindow = () => {

 window = new BrowserWindow({ width: 800, height: 600 })

 window.loadURL('https://medium.com/')

 window.on('closed', () => {

 window = null

 })

}

app.on('activate', () => window === null ? createWindow : null)

app.on('ready', createWindow)

‌

`activate` event is called if :

‌

*   the app is launch for the first time,

*   we try to launch it when is already launch

*   when we click on the icon app


‌

`ready` event is called when the initialization is done.

‌

`BrowserWindow` method of electron launch configuration of the window

your-project-directory: electron .

‌

Start


---------

‌

Create an `index.html` page and include :

<!DOCTYPE html>

<html lang="fr">

 <head>

 <meta charset="utf-8">

 <title>My first desktop application</title>

 </head>

 <body>

 <h1>Electron</h1>

 <p>It's really the best tutorial</p>

 </body>

</html>

‌

The goal is to display this page in the electron application.

‌

In `electron.js`, replace the contents of `window.loadURL( )` by :

window.loadURL(\`file://${path.join(\_\_dirname, './index.html')}\`)

‌

Place to javascript client side, create a file name `render.js` and import electron.

const { ipcRenderer } = require('electron')

alert("Render OK") //Test

‌

Do not forget to import it into the HTML5 document.

<script type="text/javascript" src="./render.js"></script>

‌

Also integrate Node.JS client side.

window = new BrowserWindow({ width: 800, height: 600, webPreferences: { nodeIntegration: true } })

> The basics of the project have been realized, let's move on to the electron functionality.

‌

Create a button :

‌

**ipcRenderer** sends the value to the server side. In the Render.js file add :

button.addEventListener('click', () => {

 ipcRenderer.send('clickbutton', 'maisouestdoncornicar')

})

‌

We import ipcMain into the Electron.js file.

const {ipcMain}  = require('electron')

‌

The value sent by the customer is received and we modify it.

ipcMain.on('clickbutton', (event, arg) => {

 //Split allows you to divide a string from

 //a separator to provide an array of substrings.

 console.log(arg.split(''))

 event.sender.send('reply', arg.split(''))

})

‌

We recover the modified value on the client side (in a table)

ipcRenderer.on('reply', (event, arg) => {

 alert(arg)

})

‌

We send a notification :

ipcRenderer.on('reply', (event, arg) => {

 alert(arg)

 let notification = {

 title: 'Ceci est une alerte',

 body: arg.toString()

 }

 new window.Notification('Alert', notification)

})

‌

​[Electron.js](https://raindrop.io/collection/7476312)​
